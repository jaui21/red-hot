<?php 
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
 
namespace OSE\RedHot\Model;

use Magento\Framework\Model\AbstractModel;

class DataHot extends AbstractModel 
{
	public function _construct()
	{
		$this->_init("OSE\RedHot\Model\ResourceModel\DataHot");
	}
}
?>