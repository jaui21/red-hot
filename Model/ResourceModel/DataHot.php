<?php 
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
 
namespace OSE\RedHot\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DataHot extends AbstractDb
{

	 protected $_isPkAutoIncrement = false;

	 public function _construct()
	 {
	 $this->_init('ose_redhot','sku');
	 }
}
?>