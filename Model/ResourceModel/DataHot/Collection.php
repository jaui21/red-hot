<?php 
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace OSE\RedHot\Model\ResourceModel\DataHot;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("OSE\RedHot\Model\DataHot","OSE\RedHot\Model\ResourceModel\DataHot");
	}
}
?>