<?php 
namespace OSE\RedHot\Model\ResourceModel\DataHot;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	public function _construct()
	{
		$this->_init(
			'OSE\RedHot\Model\DataHot',
			'OSE\RedHot\Model\ResourceModel\DataHot'
		);
	}
}
?>