# Red Hot Products

## Extension Functionalities:
- Keep a running tally for how many times a Red Hot product was added to cart.
- Display the running tally on the product page.
- Adjust the flat rate shipping rate for orders that contain a Red Hot product.
- Display “x customers love this product!” where x is the counter from the new table.
- Enable flatrate shipping and set the rate to $20.
- Intercept the flat rate shipping method and change the rate from $20 to $10 for any order that contains any Red Hot product.
