<?php
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'OSE_RedHot',
    __DIR__
);