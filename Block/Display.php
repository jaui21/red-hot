<?php
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace OSE\RedHot\Block;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Registry;
use OSE\RedHot\Model\DataHot;

class Display extends \Magento\Framework\View\Element\Template
{
	protected $registry;
	protected $redhotCollection;

	public function __construct(Context $context, DataHot $redhotCollection, Registry $registry)
	{
		$this->_registry = $registry;
		$this->_redhotCollection = $redhotCollection;
		parent::__construct($context);
	}

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

	public function getCounter($sku)
	{
		$collection = $this->_redhotCollection->getCollection();
		$counter = 0;
		foreach ($collection as $hotproduct) { 
			if($hotproduct->getSku() == $sku) {
				$counter = $hotproduct->getData("add_to_cart_count");
			}
		}
		return $counter;
			
	}
}