<?php
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */


namespace OSE\RedHot\Plugin;

use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Flatrate
{
    protected $cart;
    protected $customerSession;
    protected $rateResultFactory;
    protected $rateMethodFactory;
    protected $productRepository;

    public function __construct(
        Cart $cart,
        Session $customerSession,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->_cart = $cart;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_customerSession = $customerSession;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_productRepository = $productRepository;
    }   

    public function afterCollectRates(\Magento\OfflineShipping\Model\Carrier\Flatrate $subject, $result)
    {
        $result = $this->_rateResultFactory->create();
        $shippingPrice = "10"; //custom price

        $items = $this->_cart->getQuote()->getItemsCollection();

        $redYes = "";
        foreach ($items as $item){
           $productYN = $this->_productRepository->get($item->getSku());
           if($productYN->getAttributeText("red_hot") == "Yes") {
             $redYes = "yes";
           }
        }

        if ($redYes) {
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier('flatrate');
            $method->setCarrierTitle($subject->getConfigData('title'));

            $method->setMethod('flatrate');
            $method->setMethodTitle($subject->getConfigData('name'));

            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);
            
            $result->append($method);
        }
        return $result;
    }
}