<?php
/**
 * OSE_RedHot
 *
 * PHP version 7.4
 *
 * @category Magento2-module
 * @package  OSE_RedHot
 * @author   Joey Lana
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
 
namespace OSE\Redhot\Plugin\Model\Checkout\Cart;

use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;
use OSE\RedHot\Model\DataHot;
use OSE\RedHot\Model\DataHotFactory;


class AddIncrementRedhot
{

    protected $productRepository;
    protected $redhotCollection;
    protected $hotFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        DataHot $redhotCollection,
        DataHotFactory $hotFactory
    )
    {
        $this->_productRepository = $productRepository;
        $this->_redhotCollection = $redhotCollection;
        $this->_hotFactory = $hotFactory;
    }

    public function beforeAddProduct(Cart $subject, $productInfo, $requestInfo=null)
    {
        $product = null;
        if ($productInfo instanceof Product)
        {
            $product = $productInfo;
        }

        $productYN = $this->_productRepository->get($product->getSku());

        if ($product)
        {

            $collection = $this->_redhotCollection->getCollection();
            $counter = 0;
            foreach ($collection as $hotproduct) { 
                if($hotproduct->getSku() == $product->getSku()) {
                    $counter = $hotproduct->getData("add_to_cart_count");
                }
            }
            
            if($productYN->getAttributeText("red_hot") == "Yes") {
                $newRedHot = $this->_hotFactory->create();
                $newRedHot->setData('sku', $product->getSku());
                if ($counter==0) {
                    $newRedHot->setData('add_to_cart_count', "1");
                } else {
                    $counter++;
                    $newRedHot->setData('add_to_cart_count', $counter);
                }
                $newRedHot->save();
            }

        }
        return [$productInfo, $requestInfo];
    }

}